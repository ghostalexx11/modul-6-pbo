package modul6;

// main.java
/**
 *
 * @author D205
 */
public class main {
       public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Hello World");

    }
}


// perpustakaan1.java

package modul6;

import java.util.Scanner;

/**
 *
 * @author D205
 */
public class Perpustakaan1 {
    public static void main(String[] args) {
        String[] judulBuku = new String[3];
        Scanner input = new Scanner(System.in);
        
        try {
            for (int i = 0; i >= 0; i++) {
                System.out.println("Masukan Judul Buku!");
                judulBuku[i] = input.next();
                System.out.println(judulBuku[i] + " tersimpan");
            }
        }
        catch(ArrayIndexOutOfBoundsException a) {
            System.out.println(a);
        }
        
        System.out.println("Judul Buku yang tesimpan");
        for (int i = 0; i < judulBuku.length; i++) {
            System.out.println(judulBuku[i]);
            
        }
    }
}


// perpustakaan2.java

package modul6;

import java.util.Scanner;

/**
 *
 * @author D205
 */
public class Perpustakaan2 {
    public static void main(String[] args) {
        int [] jmlHal = new int[3];
        String [] judulSkripsi = new String[3];
        Scanner input = new Scanner(System.in);
        
        try {
            judulSkripsi[0] = "Skripsi 1";
            jmlHal[0]= 30;
            judulSkripsi[1] = "Skripsi 2";
            jmlHal[1]= 40;
            judulSkripsi[2] = "Skripsi 3";
            jmlHal[2]= 30;
        } catch (java.util.InputMismatchException a) {
            System.out.println(a);
        }
        catch(ArrayIndexOutOfBoundsException b) {
            System.out.println(b);
        }
        for (int i = 0; i < 3; i++) {
            System.out.println(judulSkripsi[i] + " " + jmlHal[i]);
        }
    }
}

